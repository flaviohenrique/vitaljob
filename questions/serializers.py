# -*- coding: utf-8 -*-
from rest_framework import serializers
from .models import Question


class QuestionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Question
        fields = ('id', 'name', 'description', 'start_date', 'position', 'value', 'one_choice', 'multiple_choice', )