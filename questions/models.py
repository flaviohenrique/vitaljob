from django.db import models

GENDER_CHOICES = (
    ('M', 'Male'),
    ('F', 'Female'),
)


class Choice(models.Model):
    description = models.CharField(max_length=50)

    def __unicode__(self):
        return self.description


class Question(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    start_date = models.DateField()
    position = models.IntegerField()
    value = models.FloatField()
    one_choice = models.CharField(max_length=1, choices=GENDER_CHOICES)
    multiple_choice = models.ManyToManyField(Choice)

    def __unicode__(self):
        return self.name

