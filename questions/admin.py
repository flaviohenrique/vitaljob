from questions.models import Question, Choice
from django.contrib import admin

admin.site.register(Question)
admin.site.register(Choice)