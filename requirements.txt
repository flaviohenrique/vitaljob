Django==1.4
django-filter==0.13.0
djangorestframework==2.3.0
Markdown==2.6.6
psycopg2==2.6.1
six==1.10.0
wheel==0.24.0
