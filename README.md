Desafio VitalJob
===================================

Introduction
============

CRUD Básico - Cadastro completo de campos de uma tabela de banco de dados (Listagem, Exibição, Inserção, Atualização e Remoção)

Deverá ser desenvolvido um CRUD contendo pelo menos um campo de cada um dos seguintes tipo de dados: Data, texto curto, 
texto longo, valor inteiro, valor ponto flutuante, seleção de valores (um somente), seleção de valores (múltiplos).

Requirements
============

* python >= 2.7
* virtualenv >= 1.7.2
* virtualenvwrapper >= 3.5
* postgresql
* e todos os pacotes do arquivo requirements.txt


Setup your local environment
============================

    mkvirtualenv vitaljob
    workon vitaljob


Install the required python packages.

    make pip


Create the tables structure (see the next item)

## DB

Criar banco de dados postgres com nome "vitaljob" e usuário "vitaljob" sem senha.

Logo em seguida executa o comando abaixo:

    make run_syncdb


## Running the project

    make run

In your browser open the URL: http://localhost:8000/admin/

And front-end URL:  http://localhost:8000/

